const express = require('express');
const app = express();
const port = 8000;
const Vigenere = require('caesar-salad').Vigenere;
const password = 'mathematics';

app.get('/', (req, res) => {
    res.send('Enter something in the url string after "/"\n');
});

app.get('/encode/:encode', (req, res) => {
    res.send(Vigenere.Cipher(password).crypt(`${req.params.encode}`));
});

app.get('/decode/:decode', (req, res) => {
    res.send(Vigenere.Decipher(password).crypt(`${req.params.decode}`));
});

app.get('/:route', (req, res) => {
    res.send(`<h1>${req.params.route}</h1>\n`);
});

app.listen(port, () => {
    console.log('We are living on ' + port);
});
